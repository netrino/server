module gitlab.com/elixxir/server

go 1.17

require (
	github.com/InfiniteLoopSpace/go_S-MIME v0.0.0-20181221134359-3f58f9a4b2b6
	github.com/cznic/mathutil v0.0.0-20181122101859-297441e03548
	github.com/golang/protobuf v1.5.2
	github.com/jinzhu/copier v0.0.0-20201025035756-632e723a6687
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.1.1
	github.com/spf13/jwalterweatherman v1.1.0
	github.com/spf13/viper v1.7.1
	gitlab.com/elixxir/comms v0.0.4-0.20220222221859-c12e29bde218
	gitlab.com/elixxir/crypto v0.0.7-0.20220222221347-95c7ae58da6b
	gitlab.com/elixxir/gpumathsgo v0.1.1-0.20211022205007-0c24acf81844
	gitlab.com/elixxir/primitives v0.0.3-0.20220222212109-d412a6e46623
	gitlab.com/xx_network/comms v0.0.4-0.20220222212058-5a37737af57e
	gitlab.com/xx_network/crypto v0.0.5-0.20220222212031-750f7e8a01f4
	gitlab.com/xx_network/primitives v0.0.4-0.20220222211843-901fa4a2d72b
	golang.org/x/crypto v0.0.0-20220128200615-198e4374d7ed
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.27.1
	gopkg.in/yaml.v2 v2.4.0
	gorm.io/driver/postgres v1.1.2
	gorm.io/gorm v1.21.16
)

require (
	github.com/gopherjs/gopherjs v0.0.0-20200217142428-fce0ec30dd00 // indirect
	github.com/magiconair/properties v1.8.4 // indirect
	github.com/mitchellh/mapstructure v1.4.0 // indirect
	github.com/pelletier/go-toml v1.8.1 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20200410134404-eec4a21b6bb0 // indirect
	github.com/smartystreets/assertions v1.2.0 // indirect
	github.com/spf13/afero v1.5.1 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	google.golang.org/genproto v0.0.0-20210105202744-fe13368bc0e1 // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
)
